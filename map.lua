require 'entities'

Game.Tilemap = function(w, h)
    local self = {}
    self.w = w
    self.h = h
    -- self.tiles is a 2d array holding all tiles
    self.tiles = {}
    self.rooms = {}
    self.doors = {}
    self.room_count = 0

    self.set = function (x, y, tile_type, room_id)
        local tile = Game.Tile(x, y, tile_type)
        tile.parent_room = room_id
        self.tiles[(y * self.w) + x] = tile
    end

    self.get = function (x, y)
        return self.tiles[(y * self.w) + x]
    end

    -- getVacantTile is used to find a random floor tile which may have an item
    -- or creature placed on it 
    self.getVacantTile = function ()
	    local tileFound = false
	    local x
        local y
	    while tileFound == false do
	    	x = math.floor(math.random() * self.w)
	    	y = math.floor(math.random() * self.h)
	    	if self.get(x, y).active then
	    		if (self.get(x, y).solid == false) then
	    			tileFound = true
	    		end
	    	end
	    end
	    return {x = x, y = y}
	end

    self.isVisible = function (tx, ty, px, py)
        local delta_x = px - tx
        local delta_y = py - ty

        local abs_delta_x = math.abs(delta_x)
        local abs_delta_y = math.abs(delta_y)

        sign_x = math.sign(delta_x)
        sign_y = math.sign(delta_y)

        local x = tx
        local y = ty

        if (abs_delta_x > abs_delta_y) then
            local t = (abs_delta_y * 2) - abs_delta_x
            while self.get(x, y).solid == false do
                if t >= 0 then
                    y = y + sign_y
                    t = t - (abs_delta_x * 2)
                end

                x = x + sign_x
                t = t + (abs_delta_y * 2)

                if x == px and y == py then
                    return true
                end
            end
            return false
        else
            local t = (abs_delta_x * 2) - abs_delta_y
            while self.get(x, y).solid == false do
                if t >= 0 then
                    x = x + sign_x
                    t = t - (abs_delta_y * 2)
                end

                y = y + sign_y
                t = t + (abs_delta_x * 2)

                if x == px and y == py then
                    return true
                end
            end
            return false
        end
    end

    -- wallUp should be used after the map configuration has been completed.
    -- It loops through the current map, and makes sure it is encased in walls
    -- so that the empty parts may not be accessed. Empty tiles which are
    -- adjacent to floor tiles will be turned into walls.
    self.wallUp = function ()
        for y = 0, self.h - 1 do
        	for x = 0, self.w - 1 do
        		if self.get(x, y).active == false then
	        		--Check surrounding tiles to see if they are active
	        		for j=-1,1 do
	        			for i=-1,1 do
	        				local dx = x + i
	        				local dy = y + j
	        				if (j ~= 0 and i ~= 0) and
	        				   dx >= 0 and dy >= 0 and
	        				   dx < self.w and dy < self.h then
                                local tile = self.get(dx, dy)
	        				    if tile.active then
	        				    	if self.get(dx, dy).solid == false then
                                        local parent = self.get(x, y).parent_room
	        				    	    self.set(x, y, Game.WALL, parent)
                                        
	        				        end
	        				    end
	        				end
	        			end
	        		end
	        	end
        	end
        end
    end

    -- digTunnel is used to connect two rooms together. It takes the center
    -- point of each room, and makes a corrider between the two
    local digTunnel = function (p1, p2)
        while (p1.x ~= p2.x) do
        	if p1.x < p2.x then
        		p1.x = p1.x + 1
        	elseif p1.x > p2.x then
        		p1.x = p1.x - 1
        	end
            self.set(p1.x, p1.y, Game.FLOOR)
            self.set(p1.x, p1.y + 1, Game.FLOOR)
        end
        while (p1.y ~= p2.y) do
            if p1.y < p2.y then
                p1.y = p1.y + 1
            elseif p1.y > p2.y then
                p1.y = p1.y - 1
            end
            local t1 = self.get(p1.x, p1.y)
            local t2 = self.get(p1.x - 1, p1.y)
            self.set(p1.x, p1.y, Game.FLOOR)
            self.set(p1.x - 1, p1.y, Game.FLOOR)
        end
    end

    -- digRoom basically just mashes a bunch of squares of floor tiles together
    -- to create an irregular shaped room
    self.digRoom = function (x, y, w, h, include_walls, room_id)
        local include_walls = include_walls or false
        for j = y, y + h - 1 do
        	for i = x, x + w - 1 do
                if include_walls and
                   (j == y or i == x or j == y + h - 1 or i == x + w - 1) and
                   self.get(i, j).active == false then
                    self.set(i, j, Game.WALL, room_id)
                else
        	        self.set(i, j, Game.FLOOR, room_id)
                end
        	end
        end
    end

    self.digTemplate = function(x, y, w, h, template_string, room_id)
        for j = 0, h - 1 do
            for i = 0, w - 1 do
                local pos = ((j * w) + i) + 1
                local char = string.sub(template_string, pos, pos)
                if char == "#" then
                    self.set(i + x, j + y, Game.WALL, room_id)
                elseif char == "." then
                    self.set(i + x, j + y, Game.FLOOR, room_id)
                else
                    print(char)
                end
            end
        end
    end


    self.clear = function ()
        for y = 0, self.h - 1 do
            for x = 0, self.w - 1 do
                self.tiles[(y * self.w) + x] = {active = false}
            end
        end
    end


    self.minimap = {}

    self.generateMini = function (scale)
        local scale = scale or 1
	    local minimapData = love.image.newImageData(self.w * scale, self.h * scale)
        mini_tile = function (x, y, r, g, b)
            for j = 0, scale - 1 do
                for i = 0, scale - 1 do
                    minimapData:setPixel(
                        (x * scale) + i,
                        (y * scale) + j,
                        r, g, b, 1)
                end
            end
        end
        for y = 0, self.h - 1 do
            for x = 0, self.w - 1 do
                local tile = self.get(x, y)

                if tile.type == Game.WALL then
                    mini_tile(x, y, 0.09, 0.16, 0.32)
                elseif tile.type == Game.FLOOR then
                    mini_tile(x, y, 0.76, 0.76, 0.78)
                elseif tile.type == Game.DOOR then
                    mini_tile(x, y, 0, .9, 0)
                else
                    mini_tile(x, y, 0, 0, 0)
                end
            end
        end
        self.minimap = love.graphics.newImage(minimapData)
    end
	return self
end

