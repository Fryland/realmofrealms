require 'game'
require 'map'
require 'entities'
require 'zdungeon'

love.window.setMode(Game.windowWidth, Game.windowHeight)

local step = (function ()
    msDelta = 0
    return (function ()
        msDelta = msDelta + love.timer.getDelta()
        if msDelta >= 0.017 then
            msDelta = 0.017 - msDelta
            return true
        else
            return false
        end
    end)
end)()

local translation = {
    active = false,
    x = 0,
    y = 0,
    room = {},
    sx = 48,
    sy = 32,
    dx = 0,
    dy = 0,
    droom = {}
}

local p
local light

function love.load()
    Game.map = Game.createZDungeon()
    Game.map.generateMini(2)
    local start_position = Game.map.getVacantTile()
    p = Game.Player(start_position.x * Game.cellWidth, start_position.y * Game.cellWidth)
    table.insert(Game.entities, p)
    for i = 0, 100 do
        local chicken_position = Game.map.getVacantTile()
        table.insert(Game.entities, Game.Chicken(chicken_position.x * Game.cellWidth, chicken_position.y * Game.cellWidth))
    end
    for k,v in pairs(Game.map.rooms) do
        local tile_x = math.floor(p.x / Game.cellWidth)
        local tile_y = math.floor(p.y / Game.cellHeight)
        if Game.collides(v, Game.Quad(tile_x, tile_y, 1, 1)) then
            p.parent_room = v
            Game.camera.focusRoom(p.parent_room)
            break
        end
    end
end
local ccount = 0

function love.update ()
    if love.keyboard.isDown("escape") then
        love.event.quit()
    end

    if step() then
        if translation.active == true then
            if translation.x < translation.dx then
                translation.x = translation.x + translation.sx
                if translation.x > translation.dx then
                    translation.x = translation.dx
                end
            end
            if translation.x > translation.dx then
                translation.x = translation.x - translation.sx
                if translation.x < translation.dx then
                    translation.x = translation.dx
                end
            end
            if translation.y < translation.dy then
                translation.y = translation.y + translation.sy
                if translation.y > translation.dy then
                    translation.y = translation.dy
                end
            end
            if translation.y > translation.dy then
                translation.y = translation.y - translation.sy
                if translation.y < translation.dy then
                    translation.y = translation.dy
                end
            end
            if translation.x == translation.dx and
               translation.y == translation.dy then
                Game.camera.focusRoom(p.parent_room)
                translation.active = false
            end
            return
        end
        p.vx = 0
        p.vy = 0
        if love.keyboard.isDown("up") then
            p.vy = -p.speed
        end
        if love.keyboard.isDown("down") then
            p.vy = p.speed
        end
        if love.keyboard.isDown("right") then
            p.vx = p.speed
        end
        if love.keyboard.isDown("left") then
            p.vx = -p.speed
        end

        for k, v in pairs(Game.entities) do
            v.step()
        end
        
        local handle_collisions = function (e, tiles, axis)
            for kta, t in pairs(tiles) do
                if t.solid then
                    while Game.collides(t, e) do
                        if axis == Game.HORIZONTAL then
                            if e.vx > 0 then
                                e.x = e.x - 1
                            elseif e.vx < 0 then
                                e.x = e.x + 1
                            end
                        elseif axis == Game.VERTICAL then
                            if e.vy > 0 then
                                e.y = e.y - 1
                            elseif e.vy < 0 then
                                e.y = e.y + 1
                            end
                        end
                    end
                end
                if e == p and t.type == Game.DOOR and t.parent_room ~= e.parent_room then
                    translation.active = true
                    translation.x = Game.camera.x
                    translation.y = Game.camera.y
                    translation.room = e.parent_room
                    Game.camera.focusRoom(t.parent_room)
                    Game.camera.center(e)
                    e.x = t.x
                    e.y = t.y
                    translation.dx = Game.camera.x
                    translation.dy = Game.camera.y
                    Game.camera.boundaries = false
                    Game.camera.x = translation.x
                    Game.camera.y = translation.y
                end
            end
        end

        for entity_key, entity in pairs(Game.entities) do
            local entity_tile = Game.map.get(math.floor(entity.x / Game.cellWidth), math.floor(entity.y / Game.cellWidth))

            local colliding_tiles = {}

            local newBox = {x = entity.x + entity.vx,
                            y = entity.y + entity.vy,
                            w = entity.w, h = entity.h}
            
            for y = entity_tile.tileY - 1, entity_tile.tileY + 1 do
                for x = entity_tile.tileX - 1, entity_tile.tileX + 1 do
                    local tile = Game.map.get(x, y)
                    if tile and tile.active and (tile.solid or tile.type == Game.DOOR) and Game.collides(tile, newBox) then
                        table.insert(colliding_tiles, tile)
                    end
                end
            end

            if entity.vx ~= 0 and entity.vx ~= -0 then
                entity.x = entity.x + entity.vx
                handle_collisions(entity, colliding_tiles, Game.HORIZONTAL)
            end
            if entity.vy ~= 0 and entity.vy ~= -0 then
                entity.y = entity.y + entity.vy
                handle_collisions(entity, colliding_tiles, Game.VERTICAL)
            end
            local new_entity_tile = Game.map.get(math.floor(entity.x / Game.cellWidth), math.floor(entity.y / Game.cellWidth))
            if new_entity_tile and new_entity_tile.active then
                entity.parent_room = new_entity_tile.parent_room
            end
        end
    end
end

function love.draw()
    if translation.active == false then
        Game.camera.center(p)
    else
        Game.camera.x = translation.x
        Game.camera.y = translation.y
    end
    local pc = p.getCenter()
    for y=p.parent_room.y, p.parent_room.bottom() do
        for x=p.parent_room.x, p.parent_room.right() do
            local tile = Game.map.get(x, y)
            if tile and tile.active then
                tile.draw()
            end
        end
    end
    if translation.active == true then
        for y=translation.room.y, translation.room.bottom() do
            for x=translation.room.x, translation.room.right() do
                local tile = Game.map.get(x, y)
                if tile and tile.active then
                    tile.draw()
                end
            end
        end
    end

    --love.graphics.setShader()
    for k, v in pairs(Game.entities) do
        if v.parent_room == p.parent_room then
            v.draw()
        end
    end
    p.draw()
    
    local minimapPosition = {
        x = Game.windowWidth - Game.map.minimap:getWidth(),
        y = Game.windowHeight - Game.map.minimap:getHeight()
    }
    local playerMarker = {
        x = minimapPosition.x + ((p.x / Game.cellWidth) * 2),
        y = minimapPosition.y + ((p.y / Game.cellWidth) * 2),
        w = 2,
        h = 2
    }
    love.graphics.setColor(1, 1, 1, .5)
    love.graphics.draw(
        Game.map.minimap,
        minimapPosition.x,
        minimapPosition.y)
    love.graphics.setColor(1, 0, 0, 1)
    love.graphics.rectangle(
        "fill",
        playerMarker.x,
        playerMarker.y,
        playerMarker.w,
        playerMarker.h)

    love.graphics.print("FPS: " .. tostring(love.timer.getFPS()), 0, 0)
end
