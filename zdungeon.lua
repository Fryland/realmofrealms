require 'map'

Game.roomTemplates = {}
Game.roomTemplates[0] = string.gsub(
[[
     #####     
   ###...###   
 ###.......##  
##..........###
#.............#
###..........##
  ##......#### 
   ###..###    
     ####      ]], "\n", "")

Game.createZDungeon = function ()
    local self = {}

    self.rooms = {}
    self.doors = {}
    local min_room_width = 15
    local min_room_height = 9
    local min_x = 0
    local min_y = 0
    local max_x = 0
    local max_y = 0

    local update_min_max = function (new_room)
        if new_room.x < min_x then
            min_x = new_room.x
        end
        if new_room.y < min_y then
            min_y = new_room.y
        end
        if new_room.right() > max_x then
            max_x = new_room.right()
        end
        if new_room.bottom() > max_y then
            max_y = new_room.bottom()
        end
    end

    for i = 0, 16 - 1 do
        local room = Game.Quad(0, 0, 0, 0)
        local tries = 0
        local sucess = false
        while tries < 20 do
            local parent_door = {}
            local new_door = {}
                
            room.w = min_room_width * (math.floor(math.random() * 2) + 1)
            room.h = min_room_height * (math.floor(math.random() * 2) + 1)
            if i == 0 then
                room.x = 0
                room.y = 0
                update_min_max(room)
                self.rooms[i] = room
                break
            end

            local parent_room = self.rooms[math.floor(math.random() * (i - 1))]

            local chance = math.random()
            --NORTH
            if chance < .25 then
                room.x = parent_room.x
                room.y = parent_room.y - room.h
                parent_door.x = parent_room.x + 7
                parent_door.y = parent_room.y
                parent_door.points = Game.NORTH
                new_door.x = parent_door.x
                new_door.y = parent_door.y - 1
                new_door.points = Game.SOUTH
            elseif chance < .5 then
                room.x = parent_room.right() + 1
                room.y = parent_room.y
                parent_door.x = parent_room.right()
                parent_door.y = parent_room.y + 4
                parent_door.points = Game.EAST
                new_door.x = parent_door.x + 1
                new_door.y = parent_door.y
                new_door.points = Game.WEST
            elseif chance < .75 then
                room.x = parent_room.x
                room.y = parent_room.bottom() + 1
                parent_door.x = parent_room.x + 7
                parent_door.y = parent_room.bottom()
                parent_door.points = Game.SOUTH
                new_door.x = parent_door.x
                new_door.y = parent_door.y + 1
                new_door.points = Game.NORTH
            else
                room.x = parent_room.x - room.w
                room.y = parent_room.y
                parent_door.x = parent_room.x
                parent_door.y = parent_room.y + 4 
                parent_door.points = Game.WEST
                new_door.x = parent_door.x - 1
                new_door.y = parent_door.y
                new_door.points = Game.EAST
            end
            local good = true
            if i > 0 then
                for j = 0, i - 1 do
                    local r = self.rooms[j]
                    if Game.collides(room, r) then
                        good = false
                        break
                    end
                end
            end
            if good then
                update_min_max(room)
                self.rooms[i] = room
                self.doors[(i * 2) - 2] = {x = parent_door.x, y = parent_door.y, parent_room = parent_room}
                self.doors[((i * 2) - 2) + 1] = {x = new_door.x, y = new_door.y, parent_room = room}
                tries = 23
            end
        end
    end
    local new_self = Game.Tilemap(1 + max_x - min_x, 1 + max_y - min_y)
    new_self.clear()
    new_self.rooms = self.rooms
    local room_id = 0
    while room_id < 16 do
        if self.rooms[room_id] then
            local r = self.rooms[room_id]
            r.x = r.x - min_x
            r.y = r.y - min_y
            if r.w == 15 and r.h == 9 and math.random() < .25 then
                new_self.digTemplate(r.x, r.y, 15, 9, Game.roomTemplates[0], r)
            else
                new_self.digRoom(r.x, r.y, r.w, r.h, true, r)
            end
            if room_id > 0 then
                local door_a = self.doors[(room_id * 2) - 2]
                local door_b = self.doors[((room_id * 2) - 2) + 1]
                door_a.x = door_a.x - min_x
                door_a.y = door_a.y - min_y
                door_b.x = door_b.x - min_x
                door_b.y = door_b.y - min_y
                new_self.set(door_a.x, door_a.y, Game.DOOR, door_a.parent_room)
                new_self.get(door_a.x, door_a.y).to = {
                    x = door_b.x * Game.cellWidth,
                    y = door_b.x * Game.cellWidth}
                new_self.set(door_b.x, door_b.y, Game.DOOR, door_b.parent_room)
                new_self.get(door_b.x, door_b.y).to = {
                    x = door_a.x * Game.cellWidth,
                    y = door_a.y * Game.cellWidth}
            end
        end
        room_id = room_id + 1
    end

    new_self.wallUp()
    return new_self
end
