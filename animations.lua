require 'game'

Game.Spritesheet = function (image_filename, tile_width, tile_height)
    local self = {}
    self.tile_width = tile_width
    self.tile_height = tile_height
    self.image_file = love.graphics.newImage(image_filename)
    self.sprites = {}
    local image_width_in_tiles = self.image_file.getWidth(self.image_file) / self.tile_width
    local image_height_in_tiles = self.image_file.getHeight(self.image_file) / self.tile_height
    for y = 0, image_height_in_tiles - 1 do
        for x = 0, image_width_in_tiles - 1 do
            local sprite_id = (y * image_width_in_tiles) + x
            self.sprites[sprite_id] = love.graphics.newQuad(
                x * self.tile_width,
                y * self.tile_height,
                self.tile_width,
                self.tile_height,
                self.image_file)
        end
    end

    self.draw = function (sprite_id, x, y, opacity)
        local opacity = opacity or 1
        love.graphics.setColor(1, 1, 1, opacity)
        love.graphics.draw(self.image_file, self.sprites[sprite_id], x, y)
    end

    return self
end

Game.Animation = function (spritesheet, steps_between_frames, frame_sequence)
    local self = {}
    current_frame = 1
    steps_since_last_frame = 0

    self.step = function ()
        steps_since_last_frame = steps_since_last_frame + 1
        if steps_since_last_frame == steps_between_frames then
            steps_since_last_frame = 0
            current_frame = current_frame + 1
            if frame_sequence[current_frame] == nil then
                current_frame = 1
            end
        end
    end

    self.draw = function(x, y)
        spritesheet.draw(frame_sequence[current_frame], x, y)
    end

    return self
end
