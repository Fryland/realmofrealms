require 'map'
Game.createMine = function (number_of_columns,
                            number_of_rows,
                            min_room_width,
                            min_room_height)
    local self = Game.Tilemap(
        (number_of_columns * min_room_width) + (number_of_columns * 2),
        (number_of_rows * min_room_height) + (number_of_rows * 2))

    local rooms = {}
    local room_count = 0

    for room_y = 0, number_of_rows - 1 do
        for room_x = 0, number_of_columns - 1 do
            local left = (room_x * min_room_width) + (room_x * 2) + 1
            local top = (room_y * min_room_height) + (room_y * 2) + 1
            local width = min_room_width
            local height = min_room_height
            if math.random() < 0.5 and room_x < number_of_columns - 1 then
                width = (min_room_width + 1) * 2
            end
            if math.random() < 0.5 and room_y < number_of_rows - 1 then
                height = (min_room_height + 1) * 2
            end
            if math.random() < 0.25 then
                local this_room = Game.Quad(left, top, min_room_width, min_room_height)
                rooms[room_count] = this_room
                self.digRoom(left, top, width, height)
                room_count = room_count + 1
            end
        end
    end

        

    for i = 0, room_count - 1 do
        local this_room = rooms[i]
        --find a room to connect to
        local connecting_id = i
        while connecting_id == i do
            connecting_id = math.floor(math.random() * room_count)
        end
        connecting_room = rooms[connecting_id]
        local p1 = connecting_room.getCenter()
        local p2 = this_room.getCenter()
        while p1.x ~= p2.x do
            if p1.x < p2.x then
                p1.x = p1.x + 1
            elseif p1.x > p2.x then
                p1.x = p1.x - 1
            end
            self.set(p1.x, p1.y, Game.FLOOR)
        end
        while p1.y ~= p2.y do
            if p1.y < p2.y then
                p1.y = p1.y + 1
            elseif p1.y > p2.y then
                p1.y = p1.y - 1
            end
            self.set(p1.x, p1.y, Game.FLOOR)
        end
    end
    self.wallUp()
    return self
end
