require 'game'
require 'animations'

Game.Entity = function (x, y)
	local self = Game.Quad(x, y, Game.cellWidth, Game.cellWidth)
	self.active = true
	self.w = Game.cellWidth
	self.h = Game.cellWidth
    self.vx = 0
    self.vy = 0
    self.facing = Game.SOUTH
    self.sprite_offset = {x = 0, y = 0}
    self.speed = 8
    self.parent_room = 0
    self.step = function () end

	self.graphicX = function ()
	    return self.x - Game.camera.x + self.sprite_offset.x
	end

    self.graphicY = function ()
    	return self.y - Game.camera.y + self.sprite_offset.y
    end

    self.step = function ()
    end

	return self
end

Game.Tile = (function ()
    local tilesheet = Game.Spritesheet("tiles.png", Game.cellWidth, Game.cellHeight)
    
    return function (tileX, tileY, type)
        local self = Game.Entity(Game.cellWidth * tileX, Game.cellWidth * tileY)
        self.type = type
        self.solid = false
        self.sprite_id = 0
        self.seen = false
        self.parent_room = 0
        if self.type == Game.WALL then
            self.sprite_id = 2
        end
        if self.type == Game.FLOOR then
            self.sprite_id = 1
        end
        self.tileX = tileX
        self.tileY = tileY
        if self.type == Game.WALL or
           self.type == Game.ROCK or
           self.type == Game.DOWNSTAIR then
               self.solid = true
        end

        function self.draw (opacity)
            local opacity = opacity or 1
            if self.type == Game.WALL then
                tilesheet.draw(self.sprite_id, self.graphicX(), self.graphicY(), opacity)
            elseif self.type == Game.FLOOR then
                tilesheet.draw(self.sprite_id, self.graphicX(), self.graphicY(), opacity)
            elseif self.type == Game.GRASS then
                tilesheet.draw(0, self.graphicX(), self.graphicY(), opacity)
            elseif self.type == Game.ROCK then
                tilesheet.draw(2, self.graphicX(), self.graphicY(), opacity)
            elseif self.type == Game.DOWNSTAIR then
                tilesheet.draw(4, self.graphicX(), self.graphicY(), opacity)
            elseif self.type == Game.DOOR then
                tilesheet.draw(0, self.graphicX(), self.graphicY(), opacity)
            end
        end

        return self
    end
end)()

Game.Player = (function ()
    local playerSprite = Game.Spritesheet("wizardguy.png", 48, 48)
    local frameDuration = 15
    local playerAnimations = {}
    playerAnimations[Game.NORTH] = Game.Animation(playerSprite, frameDuration,
        {0})
    playerAnimations[Game.EAST] = Game.Animation(playerSprite, frameDuration,
        {0})
    playerAnimations[Game.SOUTH] = Game.Animation(playerSprite, frameDuration,
        {0})
    playerAnimations[Game.WEST] = Game.Animation(playerSprite, frameDuration,
        {0})

    return function (startX, startY)
        local self = Game.Entity(startX, startY)
        self.w = Game.cellWidth
        self.h = Game.cellWidth
        self.step = function ()
            if self.vy < 0 then
                self.facing = Game.NORTH
            end
            if self.vy > 0 then
                self.facing = Game.SOUTH
            end
            if self.vx < 0 then
                self.facing = Game.WEST
            end
            if self.vx > 0 then
                self.facing = Game.EAST
            end
            if self.vx ~= 0 or self.vy ~= 0 then
                for i = Game.NORTH, Game.WEST do
                    playerAnimations[i].step()
                end
            end
        end

        function self.draw ()
            playerAnimations[self.facing].draw(self.graphicX(), self.graphicY())
        end

        return self
    end
end)()

Game.Chicken = (function ()
    local chickenSprite = Game.Spritesheet("chicken.png", 24, 22)
    local STAND = 0
    local MOVE = 1
    local JUMP = 2

    return function (startX, startY)
        local self = Game.Entity(startX, startY)
        self.w = 24
        self.h = 22
        self.speed = 6
        self.action = STAND
        self.action_duration = 0
        self.facing = Game.NORTH
        self.move_distance = 0
        self.step = function ()
            self.action_duration = self.action_duration + 1
            if self.action == STAND then
                if self.action_duration > 30 then
                    self.action = math.floor(math.random() * 3)
                    self.action_duration = -1
                end
            elseif self.action == MOVE then
                if self.action_duration == 0 then
                    self.facing = math.floor(math.random() * 4) + 1
                    self.move_distance = math.floor(math.random() * 4) + 1
                end
                if self.action_duration < 8 * self.move_distance then
                    local direction_delta = Game.directionDelta(self.facing)
                    self.vx = direction_delta.x * self.speed
                    self.vy = direction_delta.y * self.speed
                else
                    self.vx = 0
                    self.vy = 0
                    self.action_duration = -1
                    self.action = math.floor(math.random() * 3)
                end
            elseif self.action == JUMP then
                if self.action_duration < 4 then
                    self.y = self.y - 1
                elseif self.action_duration < 8 then
                    self.y = self.y + 1
                else
                    self.action_duration = -1
                    self.action = math.floor(math.random() * 3)
                end
            end
        end

        function self.draw (opacity)
            local opacity = opacity or 1
            chickenSprite.draw(0, self.graphicX(), self.graphicY(), opacity)
        end

        return self
    end
end)()
