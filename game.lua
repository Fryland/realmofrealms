Game = (function ()
    local self = {}
    self.seed = math.randomseed(os.time())

    self.cellWidth = 48
    self.cellHeight = 48
    self.entities = {}
    self.windowWidth = 720
    self.windowHeight = 432
    self.map = {}
    self.WALL = 1
    self.FLOOR = 2
    self.GRASS = 3
    self.ROCK = 4
    self.UPSTAIR = 5
    self.DOWNSTAIR = 6
    self.DOOR = 7
    self.NORTH = 1
    self.EAST = 2
    self.SOUTH = 3
    self.WEST = 4
    self.HORIZONTAL = 1
    self.VERTICAL = 2

    self.entities = {}


    self.camera = (function ()
        local camera = {}
    	camera.x = 0
    	camera.y = 0
        camera.boundaries = false
    	camera.center = function (target)
    	    camera.x = target.x + (target.w / 2) - math.floor(self.windowWidth / 2)
    	    camera.y = target.y + (target.h / 2) - math.floor(self.windowHeight / 2)
            if camera.boundaries ~= false then
                if camera.x < camera.boundaries.left then
                    camera.x = camera.boundaries.left
                end
                if camera.y < camera.boundaries.top then
                    camera.y = camera.boundaries.top
                end
                if camera.x > camera.boundaries.right - self.windowWidth then
                    camera.x = camera.boundaries.right - self.windowWidth
                end
                if camera.y > camera.boundaries.bottom - self.windowHeight then
                    camera.y = camera.boundaries.bottom - self.windowHeight
                end
            end
    	end

        camera.focusRoom = function (room)
            camera.boundaries = {}
            local room_pixel_left = room.x * self.cellWidth
            local room_pixel_top = room.y * self.cellWidth
            local room_pixel_right = (room.right() * self.cellWidth) + self.cellWidth
            local room_pixel_bottom = (room.bottom() * self.cellHeight) + self.cellHeight
            local room_pixel_width = room.w * self.cellWidth
            local room_pixel_height = room.h * self.cellHeight
            if room_pixel_width < self.windowWidth then
                camera.boundaries.left = room_pixel_left - ((self.windowWidth - room_pixel_width) / 2)
                camera.boundaries.right = camera.boundaries.left + self.windowWidth
            else
                camera.boundaries.left = room_pixel_left
                camera.boundaries.right = room_pixel_right
            end
            if room_pixel_height < self.windowHeight then
                camera.boundaries.top = room_pixel_top - ((self.windowHeight - room_pixel_height) / 2)
                camera.boundaries.bottom = camera.boundaries.top + self.windowHeight
            else
                camera.boundaries.top = room_pixel_top
                camera.boundaries.bottom = room_pixel_bottom
            end
        end

    	return camera
    end)()

    self.translateCoord = function (coord)
        return {x = coord.x - self.camera.x, y = coord.y - self.camera.y}
    end

    self.collides = function (e1, e2)
        return e1.x < e2.x + e2.w and
               e2.x < e1.x + e1.w and
               e1.y < e2.y + e2.h and
               e2.y < e1.y + e1.h
    end

    return self
end)()

Game.Quad = function (x, y, w, h)
    local self = {}
    self.x = x
    self.y = y
    self.w = w
    self.h = h

    self.getCenter = function ()
        return {x = math.floor(self.x + self.w / 2), 
                y = math.floor(self.y + self.h / 2)}
    end

    self.getRandomPoint = function ()
        return {x = math.floor(math.random() * self.w) + self.x,
                y = math.floor(math.random() * self.h) + self.y}
    end

    self.bottom = function ()
        return self.y + self.h - 1
    end

    self.right = function ()
        return self.x + self.w - 1
    end
    return self
end

Game.directionDelta = function (direction)
    if direction == Game.NORTH then
        return {x = 0, y = -1}
    elseif direction == Game.EAST then
        return {x = 1, y = 0}
    elseif direction == Game.SOUTH then
        return {x = 0, y = 1}
    elseif direction == Game.WEST then
        return {x = -1, y = 0}
    else
        print(direction, "not a direction")
        return 0
    end
end


math.sign = function (i)
    if i > 0 then
        return 1
    elseif i < 0 then
        return -1
    else
        return 0
    end
end

math.point = function (x, y)
    return {x = x, y = y}
end

math.line = function (ax, ay, bx, by)
    local self = {}
    if bx then
        self.ax = ax
        self.ay = ay
        self.bx = bx
        self.by = by
    else
        self.ax = ax.x
        self.ay = ax.y
        self.bx = ay.x
        self.by = ay.y
    end
    self.a = function ()
        return math.point(self.ax, self.ay)
    end

    self.b = function ()
        return math.point(self.bx, self.by)
    end

    self.length = function ()
        return math.abs(ax - bx)^2 + math.abs(ay - by)^2
    end

    return self
end

math.distance = function (x1, y1, x2, y2)
    return math.sqrt((x2 - x1)^2 + (y2 - y1)^2)
end


Game.bresenham = function (x1, y1, x2, y2)
    local points = {}
    local pos = {x = x1, y = y1}
    local to = {x = x2, y = y2}
    local diff = {}
    local sign = {}
    if pos.x < to.x then
        diff.x = to.x - pos.x
        sign.x = 1
    else
        diff.x = pos.x - to.x
        sign.x = -1
    end
    if pos.y < to.y then
        diff.y = to.y - pos.y
        sign.y = 1
    else
        diff.y = pos.y - to.y
        sign.y = -1
    end
    local err = diff.x - diff.y
    local e2 = nil
    local result = false
    while not(pos.x == to.x and pos.y == to.y) do
        e2 = err + err
        if e2 > -diff.y then
            err = err - diff.y
            pos.x = pos.x + sign.x
        end
        if e2 < diff.x then
            err = err + diff.x
            pos.y = pos.y + sign.y
        end
        table.insert(points, pos)
        --love.graphics.rectangle("fill", pos.x, pos.y, 1, 1)
    end
    return false
end
